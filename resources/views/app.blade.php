<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/tempusdominus-bootstrap-4.css') }}">
        <link rel="stylesheet" href="{{ asset('css/summernote-bs4.css') }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Spunta</title>
    </head>
    <body>
        @if(isset($no_menu) == false)
            @if(Auth::check())
                <div class="it-header-slim-wrapper mb-4">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="it-header-slim-wrapper-content">
                                    <a class="d-none d-lg-block navbar-brand">{{ Auth::user()->name }}</a>
                                    <div class="nav-mobile">
                                        <nav aria-label="Navigazione accessoria">
                                            <a class="it-opener d-md-none" data-bs-toggle="collapse" href="#menu1a" role="button" aria-expanded="false" aria-controls="menu4">
                                                <span class="oi oi-menu"></span>
                                            </a>
                                            <div class="link-list-wrapper collapse" id="menu1a">
                                                <ul class="link-list">
                                                    <li>
                                                        <a class="list-item" href="{{ route('checklist.index') }}">{{ _i('Checklists') }}</a>
                                                    </li>

                                                    @if(Auth::user()->hasPermission('checklists'))
                                                        <li>
                                                            <a class="list-item" href="{{ route('category.index') }}">{{ _i('Admin') }}</a>
                                                        </li>
                                                    @endif

                                                    @if(Auth::user()->hasPermission('users'))
                                                        <li>
                                                            <a class="list-item" href="{{ route('user.index') }}">{{ _i('Users') }}</a>
                                                        </li>
                                                    @endif

                                                    @if(Auth::user()->hasPermission('checklists'))
                                                        <li>
                                                            <a class="list-item" href="{{ route('history.index') }}">{{ _i('Reports') }}</a>
                                                        </li>
                                                    @endif

                                                    @if(Auth::user()->hasPermission('notices'))
                                                        <li>
                                                            <a class="list-item" href="{{ route('notice.admin') }}">{{ _i('Notices') }}</a>
                                                        </li>
                                                    @else
                                                        <li>
                                                            <a class="list-item" href="{{ route('notice.index') }}">{{ _i('Notices') }}</a>
                                                        </li>
                                                    @endif

                                                    @if(Auth::user()->hasPermission('checklists'))
                                                        <li>
                                                            <a class="list-item" href="{{ route('config.index') }}">{{ _i('Configurations') }}</a>
                                                        </li>
                                                    @endif
                                                </ul>
                                            </div>
                                        </nav>
                                    </div>
                                    <div class="it-header-slim-right-zone">
                                        <form class="form-inline" method="GET" action="{{ route('logout') }}">
                                            <button class="btn btn-outline-danger bg-white" type="submit">{{ _i('Logout') }}</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endif

        <div class="container mt-5">
            @yield('contents')
        </div>

        <script src="{{ mix('js/app.js') }}"></script>
    </body>
</html>
