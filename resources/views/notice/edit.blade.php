<?php

if (isset($notice) && $notice != null) {
    $modal_id = sprintf('editNotice-%s', $notice->id);
    $modal_label = _i('Edit Notice');
    $form_url = route('notice.update', $notice->id);
    $form_method = 'PUT';
}
else {
    $notice = null;
    $modal_id = 'createNotice';
    $modal_label = _i('Create Notice');
    $form_url = route('notice.store');
    $form_method = 'POST';
}

?>

<div class="modal fade notice-editor {{ $notice ? 'editing-notice' : '' }}" id="{{ $modal_id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ $modal_label }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{ $form_url }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="{{ $form_method }}">

                <div class="modal-body">
                    <div class="form-group">
                        <label for="title">{{ _i('Title') }}</label>
                        <input type="text" class="form-control" name="title" id="title" value="{{ $notice ? $notice->title : '' }}" required autocomplete="false">
                    </div>
                    <div class="form-group">
                        <label for="body">{{ _i('Body') }}</label>
                        <textarea class="form-control htmledit" name="body" id="body" rows="10" required autocomplete="false">{{ $notice ? $notice->body : '' }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="start" class="active">{{ _i('From') }}</label>
                        <input id="{{ $modal_id }}_from" type="date" class="form-control" name="start" id="start" value="{{ $notice ? $notice->start : '' }}" required autocomplete="false">
                    </div>
                    <div class="form-group">
                        <label for="end" class="active">{{ _i('To') }}</label>
                        <input id="{{ $modal_id }}_to" type="date" class="form-control" name="end" id="end" value="{{ $notice ? $notice->end : '' }}" required autocomplete="false">
                    </div>
                    <div class="form-group">
                        <label for="archive" class="active">{{ _i('Archive Date') }}</label>
                        <input id="{{ $modal_id }}_archive" type="date" class="form-control" name="archive" id="archive" value="{{ $notice ? $notice->archive : '' }}" autocomplete="false">
                    </div>
                    <div>
                        <div class="form-check">
                            <input type="radio" name="type" id="type_normal" value="normal" {{ (is_null($notice) || ($notice && $notice->type == 'normal')) ? 'checked' : '' }}>
                            <label for="type_normal">{{ _i('Normal') }}</label>
                        </div>
                        <div class="form-check">
                            <input type="radio" name="type" id="type_important" value="important" {{ ($notice && $notice->type == 'important') ? 'checked' : '' }}>
                            <label for="type_important">{{ _i('Important') }}</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ _i('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ _i('Save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
