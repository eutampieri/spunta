<div class="modal fade" id="destroyNotice-{{ $notice->id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ _i('Delete Notice') }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{ route('notice.destroy', $notice->id) }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <div class="modal-body">
                    <p>
                        {{ _i('Are you really sure you want to delete notice "%s"?', $notice->title) }}
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ _i('Close') }}</button>
                    <button type="submit" class="btn btn-danger">{{ _i('Delete Notice') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
