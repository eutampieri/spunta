@extends('app')

@section('contents')
    <div class="row">
        <div class="col">
            <button type="button" class="btn btn-primary float-end" data-bs-toggle="modal" data-bs-target="#createNotice">{{ _i('Create New Notice') }}</button>
            <div class="clearfix"></div>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col">
            @if($notices->isEmpty())
                <div class="alert alert-info">
                    <p>
                        {{ _i('There are not notices.') }}
                    </p>
                    <p>
                        {{ _i("Notices will appear on the first page, for all users, just after the login. Use them for communications, recommendations, informations and more!") }}
                    </p>
                </div>
            @else
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th width="30%">{{ _i('Title') }}</th>
                                <th width="10%">{{ _i('Start') }}</th>
                                <th width="10%">{{ _i('End') }}</th>
                                <th width="10%">{{ _i('Type') }}</th>
                                <th width="15%">{{ _i('Reading Users') }}</th>
                                <th width="15%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($notices as $notice)
                                <tr class="{{ $notice->archive != null && $notice->archive <= $now ? 'text-muted' : '' }}">
                                    <td>{{ $notice->title }}</td>
                                    <td>{{ printableDate($notice->start) }}</td>
                                    <td>{{ printableDate($notice->end) }}</td>
                                    <td>
                                        @if($notice->type == 'important')
                                            <span class="oi oi-warning"></span>
                                        @else
                                            <span class="oi oi-bullhorn"></span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($notice->users()->count() != 0)
                                            <a href="#" class="async-modal-edit" data-edit-url="{{ route('notice.readcount', $notice->id) }}">{{ $notice->users()->count() }}</a>
                                        @else
                                            0
                                        @endif
                                    </td>
                                    <td>
                                        <span class="action-icons float-end">
                                            <span class="oi oi-pencil async-modal-edit" title="{{ _i('Edit Notice') }}" data-edit-url="{{ route('notice.edit', $notice->id) }}"></span>
                                            <span class="oi oi-trash async-modal-edit" title="{{ _i('Delete Notice') }}" data-edit-url="{{ route('notice.askdestroy', $notice->id) }}"></span>
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col">
            {!! $notices->links() !!}
        </div>
    </div>

    @include('notice.edit', ['notice' => null])
@endsection
