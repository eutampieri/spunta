<div class="notice alert alert-dismissible alert-{{ $notice->type == 'important' ? 'warning' : 'primary' }}" role="alert" data-read-url="{{ route('notice.read', $notice->id) }}">
	<p class="display-5">{{ $notice->title }}</p>

	@if($dismiss && $notice->type != 'important')
		<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
	@endif

	<p>
		{!! nl2br($notice->body) !!}
	</p>
</div>
