{{ _i('Warning! The checklist %s has not been uploaded!', $report->checklist->name) }}

{{ _i('The reported error is: %s', $message) }}

{{ _i('For further details, get a look to the report:') }}
{{ route('history.show', $report->id) }}
