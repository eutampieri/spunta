{{ _i('Warning! The checklist %s has been filled out of time!', $report->checklist->name) }}

{{ _i('For further details, get a look to the report:') }}
{{ route('history.show', $report->id) }}
{{ _i('or contact the email address %s', $report->user->email) }}
