{{ _i('Warning! There are missing items in %s checklist!', $report->checklist->name) }}

@foreach($issues as $i)
    {{ $i }}
@endforeach

{{ _i('For further details, get a look to the report:') }}
{{ route('history.show', $report->id) }}
{{ _i('or contact the email address %s', $report->user->email) }}
