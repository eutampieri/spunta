@extends('app')

@section('contents')
    <?php

    if (isset($pending)) {
        $existing_data = json_decode($pending->as_data);
        $existing_data = $existing_data->contents ?? false;
        $pending_id = $pending->id;
    }
    else {
        $existing_data = false;
        $pending_id = 0;
    }

    ?>

    <form method="POST" action="{{ route('history.store') }}" class="checklist-create">
        @csrf

        <input type="hidden" name="checklist_id" value="{{ $checklist->id }}">
        <input type="hidden" name="pending_id" value="{{ $pending_id }}">

        <h1>{{ $checklist->name }}</h1>
        <hr>

        @foreach($checklist->slots as $slot)
            @include('history.slot', ['slot' => $slot, 'data' => $slot->existingData($existing_data)])
        @endforeach

        <hr>

        <button type="submit" class="btn btn-primary btn-lg btn-block" name="action" value="submit">{{ _i('Sign and Save') }}</button>
        <button type="submit" class="btn btn-info btn-md btn-block mt-4" name="action" value="pending">{{ _i('Temporary Save') }}</button>

        <hr>

        <a href="{{ route('checklist.index') }}" class="btn btn-sm btn-danger btn-md btn-block mt-4">{{ _i('Cancel and Back to Checklists') }}</a>

        <br>
    </form>
@endsection
