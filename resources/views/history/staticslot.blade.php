@if(isset($master->slots))
    @foreach($master->slots as $slot)
        <tr>
            <th colspan="3">
                {!! str_repeat('-', $deep * 2) !!} {{ $slot->name }}
            </th>
        </tr>
        @foreach($slot->items as $item)
            <tr class="{{ $item->status == false ? 'table-danger' : '' }}">
                <td>{{ $item->name }}</td>
                <td>{{ $item->value }}</td>
                <td>
                    @if($item->status)
                        <span class="oi oi-check"></span>
                    @else
                        <span class="oi oi-ban"></span>
                    @endif
                </td>
            </tr>
        @endforeach

        @include('history.staticslot', ['master' => $slot, 'deep' => $deep + 1])
    @endforeach
@else
    @foreach($master->contents as $content)
        @if(isset($content->contents))
            <tr>
                <th colspan="3">
                    {!! str_repeat('-', $deep * 2) !!} {{ $content->name }}
                </th>
            </tr>

            @include('history.staticslot', ['master' => $content, 'deep' => $deep + 1])
        @else
            <tr class="{{ $content->status == false ? 'table-danger' : '' }}">
                <td>{{ $content->name }}</td>
                <td>{{ $content->value }}</td>
                <td>
                    @if($content->status)
                        <span class="oi oi-check"></span>
                    @else
                        <span class="oi oi-ban"></span>
                    @endif
                </td>
            </tr>
        @endif
    @endforeach
@endif
