@extends('app')

@section('contents')
    <div class="row">
        <div class="col">
            <p>
                {{ _i('Please input your PIN to sign the filled checklist.') }}
            </p>
        </div>
    </div>

    <input type="hidden" name="privkey" id="privkey" value="{{ $user->privkey }}">
    <input type="hidden" name="cleartext" id="cleartext" value="{{ $report->as_text }}">

    <div class="row">
        <div class="col">
            <p id="please-wait" hidden>
                {{ _i('Please wait...') }}
            </p>

            <p id="error-msg" class="alert alert-danger" hidden>
                {{ _i('Invalid PIN, please retry') }}
            </p>

            <form method="PUT" action="{{ route('history.update', $report->id) }}" class="sign_document">
                <div class="form-group">
                    <label for="pin">{{ _i('PIN') }}</label>
                    <input type="password" class="form-control form-control-lg" name="pin" id="pin" required autocomplete="false" minlength="5" maxlength="5">
                </div>

                <div class="form-group row">
                    <div class="col-sm-8 col-sm-offset-4">
                        <button type="submit" class="btn btn-success btn-lg">{{ _i('Save') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <br><br><br><br>

    <div class="row">
        <div class="col">
            <span class="btn btn-danger oi oi-key async-modal-edit" title="{{ _i('Reset Sign Key') }}" data-edit-url="{{ route('user.askkeydestroy', $user->id) }}"> {{ _i('Ask key reset') }}</span>
        </div>
    </div>
@endsection
