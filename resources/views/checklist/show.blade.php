@if($checklist == null && $items->isEmpty() && $categories->isEmpty())
    <div class="row">
        <div class="col">
            <div class="alert alert-info">
                {{ _i('Here you will assign items to checklists, sort them, and organize them as you prefer.') }}
            </div>
        </div>
    </div>
@else
    <div class="row">
        <div class="col">
            <div class="alert alert-info">
                {{ _i('Drag and drop items on the preferred categories/slots to add them to the related checklists.') }}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            @if($items->isEmpty())
                <div class="alert alert-info">
                    <p>
                        {{ _i('There are not items.') }}
                    </p>
                    <p>
                        {!! _i('You have first to <a href="%s">create items</a> to be assigned to the checklist.', [route('category.index', ['tab' => 'items'])]) !!}
                    </p>
                </div>
            @else
                <input type="text" class="form-control filter-group" data-target="#disposable-items" placeholder="{{ _i('Filter Items') }}">
                <br>

                <ul class="list-group" id="disposable-items">
                    @foreach($items as $item)
                        <li class="list-group-item" data-item-id="{{ $item->id }}">
                            <span class="oi oi-arrow-right" title="{{ _i('Drag to Add') }}"></span>
                            <span class="name">{{ $item->name }}</span>
                        </li>
                    @endforeach
                </ul>
            @endif
        </div>
        <div class="col-md-6">
            @if($checklist == null)
                <div class="alert alert-warning">
                    <p>
                        {{ _i('No checklist selected.') }}
                    </p>

                    @if($categories->isEmpty())
                        <p>
                            {!! _i('You have first to <a href="%s">create a category</a> and a checklist at least.', [route('category.index', ['tab' => 'categories'])]) !!}
                        </p>
                    @else
                        @if(App\Checklist::count() == 0)
                            <p>
                                {{ _i('You have to create at least a checklist.') }}
                            </p>
                        @else
                            <p>
                                {{ _i('Click on one of the checklists on the right to activate it and populate with items.') }}
                            </p>
                        @endif
                    @endif
                </div>
            @else
                <li class="list-group-item checklist-row" data-update-url="{{ route('checklist.update', $checklist->id) }}">
                    <span class="action-icons float-end">
                        <button type="button" class="btn btn-primary float-end" data-bs-toggle="modal" data-bs-target="#createSlot-{{ $checklist->id }}">{{ _i('Create New Slot') }}</button>
                    </span>

                    <h3>{{ $checklist->name }}</h3>

                    <div class="clearfix"></div>
                    <hr>

                    <ul class="list-group sortable" data-sorted-callback="updateSlots">
                        @foreach($checklist->slots as $slot)
                            @include('slot.show', ['slot' => $slot])
                        @endforeach
                    </ul>
                </li>

                @include('slot.edit', ['checklist' => $checklist, 'slot' => null])
            @endif
        </div>
        <div class="col-md-3">
            @if($categories->isEmpty())
                <div class="alert alert-info">
                    {{ _i('There are not categories.') }}
                </div>
            @else
                <p class="text-end">
                    {{ _i('Checklists') }}
                </p>
                <div class="nav flex-column nav-pills">
                    @foreach($categories as $category)
                        <a class="list-group-item list-group-item-light">{{ $category->name }}</a>

                        @if($category->checklists->isEmpty())
                            <a class="nav-link">
                                {{ _i('There are no checklists in this category') }}
                            </a>
                        @else
                            @foreach($category->checklists as $iter_checklist)
                                <a class="nav-link {{ $checklist != null && $iter_checklist->id == $checklist->id ? 'active' : '' }}" href="{{ route('category.index', ['tab' => 'distribution', 'checklist' => $iter_checklist->id]) }}">
                                    {{ $iter_checklist->name }}
                                </a>
                            @endforeach
                        @endif
                    @endforeach
                </div>
            @endif
        </div>
    </div>

    <div id="templates" hidden>
        <ul class="item-in-slot">
            <li class="list-group-item" data-item-id="">
                <span class="name"></span>
                <span class="action-icons float-end">
                    <span class="oi oi-trash" title="{{ _i('Remove Object') }}"></span>
                    <span class="oi oi-resize-height" title="{{ _i('Drag to Sort') }}"></span>
                </span>
            </li>
        </ul>
    </div>
@endif
