<div class="modal fade" id="destroyChecklist-{{ $checklist->id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ _i('Duplicate Checklist') }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{ route('checklist.store') }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="original_checklist" value="{{ $checklist->id }}">
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="new_name">{{ _i('Name') }}</label>
                        <input type="text" class="form-control" name="name" id="new_name" value="{{ _i('Copy of %s', $checklist->name) }}" required autocomplete="false">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ _i('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ _i('Save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
