@extends('app')

@section('contents')
    <div class="row justify-content-center mt-4">
        <div class="col-12 col-md-6 text-center">
            @php
            $message = App\Config::getConfig('intro_message');
            @endphp

            @if(filled($message))
                <div class="alert alert-info mb-5">
                    {!! nl2br($message) !!}
                </div>
            @endif

            @switch(App\Config::getConfig('login'))
                @case('manual')
                    <form action="{{ route('login.manual') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="email">{{ _i('Email') }}</label>
                            <input type="email" class="form-control" name="email" id="email">
                        </div>
                        <div class="form-group">
                            <label for="password">{{ _i('Password') }}</label>
                            <input type="password" class="form-control" name="password" id="password">
                        </div>
                        <button type="submit" class="btn btn-primary">{{ _i('Login') }}</button>
                    </form>

                    @break

                @case('sso')
                    <form action="{{ route('login.sso') }}" method="POST">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-primary">{{ _i('Login') }}</button>
                        <br>
                        <br>
                        <br>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="remember" id="remember">
                            <label for="remember">{{ _i('Do not logout from Google. Check this only if not working on a shared computer.') }}</label>
                        </div>
                    </form>

                    @break
            @endswitch
        </div>
    </div>
@endsection
