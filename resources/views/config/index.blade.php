@extends('app')

@section('contents')
    <div class="row mt-4">
        <div class="col">
            <form method="POST" action="{{ route('config.update', 0) }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PUT">

                <!--
                <?php $login_type = App\Config::getConfig('login') ?>
                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="login" id="login_manual" value="manual" {{ $login_type == 'manual' ? 'checked' : '' }}>
                        <label class="form-check-label" for="login_manual">
                            {{ _i('Manual: users are manually created') }}
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="login" id="login_google" value="google" {{ $login_type == 'google' ? 'checked' : '' }}>
                        <label class="form-check-label" for="login_google">
                            {{ _i('Google: access is granted by own Google account; users are automatically generated. Warning: before enable this, Google API keys have to be stored in the configuration') }}
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="enforced_domain">{{ _i('Enforce Access to Given Mail Domain') }}</label>
                    <input type="text" class="form-control" name="enforced_domain" id="enforced_domain" value="{{ App\Config::getConfig('enforced_domain') }}" autocomplete="false">
                    <small class="form-text text-muted">{{ _i("To restrict access only to G-Suite accounts like username@mydomain.com, write here 'mydomain.com'. Recommended when Google access is enabled: if not specified, everyone with a Google account will be able to access and fill the checklists.") }}</small>
                </div>
                -->

                <div class="form-group">
                    <label for="intro_message">{{ _i('Public Message on Login Screen') }}</label>
                    <textarea class="form-control" name="intro_message" id="intro_message" autocomplete="false" rows="10" placeholder="{{ _i('Write here a message to display on the public login screen.') }}">{{ App\Config::getConfig('intro_message') }}</textarea>
                </div>

                <div class="form-group">
                    <label for="logo" class="active">{{ _i('Logo Image (to be used in PDFs)') }}</label>
                    <input type="file" class="form-control" name="logo" id="logo" accept=".jpg">

                    @if(file_exists(images_path() . 'logo.jpg'))
                        <img src="{{ route('config.image') . '?t=' . time() }}" alt="Logo" class="img-fluid">
                    @endif

                    <small class="form-text text-muted">
                        {{ _i('Only JPG files are admitted. This will not be resized, so please provide a properly sized image.') }}
                    </small>
                </div>

                <?php $language = App\Config::getConfig('language') ?>
                <div class="form-group select-wrapper">
                    <label for="language">{{ _i('Language') }}</label>
                    <select name="language" id="language">
                        <option value="it_IT" {{ $language == 'it_IT' ? 'selected' : '' }}>Italiano</option>
                        <option value="en_US" {{ $language == 'en_US' ? 'selected' : '' }}>English</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="notify_sync_reports">{{ _i('Send System Notifications to E-Mail') }}</label>
                    <input type="email" class="form-control" name="notify_sync_reports" id="notify_sync_reports" placeholder="{{ _i('This email address will receive a few system notification, such as when keys are reset for a user') }}" value="{{ App\Config::getConfig('notify_sync_reports') }}" autocomplete="false">
                </div>

                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="checklist_times" id="checklist_times" {{ App\Config::getConfig('checklist_times') ? 'checked' : '' }}>
                        <label for="checklist_times" class="form-check-label">{{ _i('Use Temporal Validation for Checklists') }}</label>
                        <small class="form-text text-muted">
                            {{ _i('When enabled, for each checklist it is possible to specify in which days and hours it is expected to be compiled. If a report is filled in different moments, an alert email is triggered.') }}
                        </small>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="self_verification" id="self_verification" {{ App\Config::getConfig('self_verification') ? 'checked' : '' }}>
                        <label for="self_verification" class="form-check-label">{{ _i('Send compiled reports to the reporter') }}</label>
                        <small class="form-text text-muted">
                            {{ _i('When enabled, each user filling a report receives a copy of the report itself by email.') }}
                        </small>
                    </div>
                </div>

                <?php

                $has_ftp = App\Config::getConfig('ftp_push');

                ?>

                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="enable_ftp_push" id="enable_ftp_push" {{ $has_ftp->enable ? 'checked' : '' }}>
                        <label for="enable_ftp_push">{{ _i('Enable FTP Push') }}</label>
                        <small class="form-text text-muted">
                            {{ _i('When enabled, each saved report is pushed to the specified FTP server. This can be used for addictional backup, convenience, or to trigger other actions.') }}
                        </small>
                    </div>
                </div>

                <div class="ftp-settings card-wrapper card-space mb-4 {{ $has_ftp->enable == false ? 'd-none' : '' }}">
                    <div class="card card-bg">
                        <div class="card-body">
                            <div class="form-group mt-3">
                                <label for="ftp_host">{{ _i('Host') }}</label>
                                <input type="text" class="form-control" name="ftp_host" id="ftp_host" value="{{ $has_ftp->host }}" autocomplete="false">
                            </div>
                            <div class="form-group">
                                <label for="ftp_username">{{ _i('Username') }}</label>
                                <input type="text" class="form-control" name="ftp_username" id="ftp_username" value="{{ $has_ftp->username }}" autocomplete="false">
                            </div>
                            <div class="form-group">
                                <label for="ftp_password">{{ _i('Password') }}</label>
                                <input type="password" class="form-control" name="ftp_password" id="ftp_password" value="{{ $has_ftp->password }}" autocomplete="false">
                            </div>
                            <div class="form-group">
                                <label for="ftp_port">{{ _i('Port') }}</label>
                                <input type="number" class="form-control" name="ftp_port" id="ftp_port" value="{{ $has_ftp->port }}" autocomplete="false">
                            </div>
                            <div class="form-group">
                                <label for="ftp_root">{{ _i('Root Folder') }}</label>
                                <input type="text" class="form-control" name="ftp_root" id="ftp_root" value="{{ $has_ftp->root }}" autocomplete="false">
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="ftp_ssl" id="ftp_ssl" {{ $has_ftp->ssl ? 'checked' : '' }}>
                                <label for="ftp_ssl">{{ _i('Enable SSL') }}</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="ftp_passive" id="ftp_passive" {{ $has_ftp->passive ? 'checked' : '' }}>
                                <label for="ftp_passive">{{ _i('Passive FTP') }}</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-8 offset-sm-4">
                        <button type="submit" class="btn btn-success float-end">{{ _i('Save') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
