@extends('app')

@section('contents')
    <div class="row">
        <div class="col">
            <button type="button" class="btn btn-primary float-end" data-bs-toggle="modal" data-bs-target="#createUser">{{ _i('Create New User') }}</button>
            <button type="button" class="btn btn-primary float-end me-2" data-bs-toggle="modal" data-bs-target="#importCSV">{{ _i('Import CSV') }}</button>
            <div class="clearfix"></div>
        </div>
    </div>

    <hr>

    @if($users->count() == 1)
        @if(App\Config::getConfig('login') == 'manual')
            <div class="alert alert-info">
                <p>
                    {{ _i('Here you can create users for the platforms, and assign permissions. Users with no permissions can only see notices and fill the existing checklists.') }}
                </p>
            </div>
        @endif
    @endif

    <div class="row">
        <div class="col">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th width="25%">{{ _i('Name') }}</th>
                            <th width="20%">{{ _i('E-Mail') }}</th>
                            <th width="10%">{{ _i('Admin Users') }}</th>
                            <th width="10%">{{ _i('Admin Checklists') }}</th>
                            <th width="10%">{{ _i('Admin Notices') }}</th>
                            <th width="25%">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr class="{{ $user->deleted_at != null ? 'text-muted' : '' }}">
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>

                                @foreach(['users', 'checklists', 'notices'] as $permission_level)
                                    <td>
                                        <input type="checkbox" class="check_switch" data-update-url="{{ route('user.setpermission') }}" data-permission="{{ $permission_level }}" data-target-id="{{ $user->id }}" {{ $user->hasPermission($permission_level) ? 'checked' : '' }} autocomplete="off" {{ $user->id == $currentuser->id || $user->deleted_at != null ? 'disabled' : '' }}>
                                    </td>
                                @endforeach

                                <td>
                                    <span class="action-icons float-end">
                                        @if(App\Config::getConfig('login') == 'manual')
                                            <span class="oi oi-pencil async-modal-edit" title="{{ _i('Edit User') }}" data-edit-url="{{ route('user.edit', $user->id) }}"></span>
                                        @endif

                                        @if($user->deleted_at == null)
                                            <span class="oi oi-trash async-modal-edit" data-edit-url="{{ route('user.askdestroy', $user->id) }}" title="{{ _i('Delete User') }}"></span>
                                        @else
                                            <a href="{{ route('user.restore', $user->id) }}"><span class="oi oi-action-undo" title="{{ _i('Re-Enable User') }}"></span></a>
                                        @endif

                                        <a href="{{ route('history.index', ['user_id' => $user->id]) }}"><span class="oi oi-list" title="{{ _i('Show Reports') }}"></span></a>

                                        @if($user->getPrivkeyAttribute() != null)
                                            <span class="oi oi-key async-modal-edit" title="{{ _i('Reset Sign Key') }}" data-edit-url="{{ route('user.askkeydestroy', $user->id) }}"></span>
                                        @endif

                                        <a href="{{ route('user.getkey', $user->id) }}"><span class="oi oi-data-transfer-download" title="{{ _i('Get Key') }}"></span></a>
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @include('user.edit', ['user' => null])

    <div class="modal fade" id="importCSV" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ _i('Import CSV') }}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{ route('user.import') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="modal-body">
                        <p>
                            {!! _i('You can import a CSV file to create multiple users in batch. Please <a href="%s">download and use this template</a> as a reference.', [url('import_template.csv')]) !!}
                        </p>

                        <div class="form-group">
                            <input type="file" class="form-control" name="file" id="file" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ _i('Close') }}</button>
                        <button type="submit" class="btn btn-success">{{ _i('Save') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
