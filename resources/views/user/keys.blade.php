@extends('app')

@section('contents')
    <div class="row">
        <div class="col">
            <p>
                {{ _i('Here we create the keys used to sign your documents. Input below a 5 digits PIN, and do not forget it: it will be asked everytime you fill a new checklist!') }}
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <p id="please-wait" hidden>
                {{ _i('Please wait...') }}
            </p>

            <form method="POST" action="{{ route('user.savekeys') }}" class="generate_keys">
                <input type="hidden" name="name" value="{{ $user->name }}">
                <input type="hidden" name="email" value="{{ $user->email }}">

                <div class="form-group row">
                    <label for="new_pin">{{ _i('PIN') }}</label>
                    <input type="password" class="form-control form-control-lg" name="pin" id="new_pin" required autocomplete="false" minlength="5" maxlength="5">
                </div>

                <div class="form-group row">
                    <div class="col-sm-8 col-sm-offset-4">
                        <button type="submit" class="btn btn-success btn-lg">{{ _i('Save') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
