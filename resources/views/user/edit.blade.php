<?php

if (isset($user) && $user != null) {
    $modal_id = sprintf('editUser-%s', $user->id);
    $modal_label = _i('Edit User');
    $form_url = route('user.update', $user->id);
    $form_method = 'PUT';
}
else {
    $notice = null;
    $modal_id = 'createUser';
    $modal_label = _i('Create User');
    $form_url = route('user.store');
    $form_method = 'POST';
}

?>

<div class="modal fade user-editor {{ $user ? 'editing-user' : '' }}" id="{{ $modal_id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ $modal_label }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{ $form_url }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="{{ $form_method }}">

                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">{{ _i('Name') }}</label>
                        <input type="text" class="form-control" name="name" id="name" value="{{ $user ? $user->name : '' }}" required autocomplete="false">
                    </div>
                    <div class="form-group">
                        <label for="email">{{ _i('E-Mail') }}</label>
                        <input type="email" class="form-control" name="email" id="email" value="{{ $user ? $user->email : '' }}" required autocomplete="false">
                    </div>
                    <div class="form-group">
                        <label for="password">{{ _i('Password') }}</label>
                        @if($user)
                            <input type="password" class="form-control" name="password" id="password" placeholder="{{ _i('Keep empty to not change current password') }}" autocomplete="false">
                        @else
                            <input type="password" class="form-control" name="password" id="password" required autocomplete="false">
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ _i('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ _i('Save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
