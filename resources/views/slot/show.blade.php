<li class="list-group-item droppable-slot" data-slot-id="{{ $slot->id }}" data-attach-url="{{ route('slot.attach', $slot->id) }}">
    <div>
        {{ $slot->name }}

        <span class="action-icons float-end">
            <span class="oi oi-plus async-modal-edit" title="{{ _i('Create New Slot') }}" data-edit-url="{{ route('slot.create', ['parent_id' => $slot->id]) }}"></span>
            <span class="oi oi-pencil async-modal-edit" title="{{ _i('Edit Slot') }}" data-edit-url="{{ route('slot.edit', $slot->id) }}"></span>
            <span class="oi oi-trash async-modal-edit" title="{{ _i('Delete Slot') }}"data-edit-url="{{ route('slot.askdestroy', $slot->id) }}"></span>
            <span class="oi oi-resize-height" title="{{ _i('Drag to Sort') }}"></span>
        </span>
    </div>

    <hr>

    <ul class="list-group sortable" data-sorted-callback="updateItemsInSlots" data-update-url="{{ route('slot.update', $slot->id) }}">
        @foreach($slot->contents as $content)
            @if(is_a($content, 'App\Slot'))
                @include('slot.show', ['slot' => $content])
            @elseif(is_a($content, 'App\Item'))
                <li class="list-group-item" data-item-id="{{ $content->id }}">
                    <span class="name">{{ $content->name }}</span>

                    <span class="action-icons float-end">
                        <span class="oi oi-trash" title="{{ _i('Remove Item') }}"></span>
                        <span class="oi oi-resize-height" title="{{ _i('Drag to Sort') }}"></span>
                    </span>
                </li>
            @endif
        @endforeach
    </ul>
</li>
