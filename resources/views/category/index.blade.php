@extends('app')

@section('contents')
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link {{ $active_tab == 'categories' ? 'active' : '' }}" data-bs-toggle="tab" href="#categories-tab" role="tab">{{ _i('Categories and Checklists') }}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ $active_tab == 'items' ? 'active' : '' }}" data-bs-toggle="tab" href="#items-tab" role="tab">{{ _i('Items') }}</a>
        </li>
        <li class="nav-item d-none d-md-block">
            <a class="nav-link {{ $active_tab == 'distribution' ? 'active' : '' }}" data-bs-toggle="tab" href="#distribution-tab" role="tab">{{ _i('Distribution') }}</a>
        </li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane fade show {{ $active_tab == 'categories' ? 'active' : '' }}" id="categories-tab" role="tabpanel">
            <br>
            @include('category.admin')
        </div>

        <div class="tab-pane fade show {{ $active_tab == 'items' ? 'active' : '' }}" id="items-tab" role="tabpanel">
            <br>
            @include('item.admin')
        </div>

        <div class="tab-pane fade show {{ $active_tab == 'distribution' ? 'active' : '' }}" id="distribution-tab" role="tabpanel">
            <br>
            @include('checklist.show', ['checklist' => App\Checklist::find($active_checklist)])
        </div>
    </div>
@endsection
