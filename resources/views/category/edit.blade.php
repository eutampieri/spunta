<?php

if (isset($category) && $category != null) {
    $modal_id = sprintf('editCategory-', $category->id);
    $modal_label = _i('Edit Category');
    $form_url = route('category.update', $category->id);
    $form_method = 'PUT';
    $actual_name = $category->name;
}
else {
    $modal_id = 'createCategory';
    $modal_label = _i('Create Category');
    $form_url = route('category.store');
    $form_method = 'POST';
    $actual_name = '';
}

?>

<div class="modal fade" id="{{ $modal_id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ $modal_label }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{ $form_url }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="{{ $form_method }}">

                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">{{ _i('Name') }}</label>
                        <input type="text" class="form-control" name="name" id="name" value="{{ $actual_name }}" required autocomplete="false">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ _i('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ _i('Save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
