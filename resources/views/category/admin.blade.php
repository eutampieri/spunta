<div class="row">
    <div class="col">
        <button type="button" class="btn btn-primary float-end" data-bs-toggle="modal" data-bs-target="#createCategory">{{ _i('Create New Category') }}</button>
        <div class="clearfix"></div>
    </div>
</div>

@if($categories->isEmpty())
    <hr>

    <div class="alert alert-info">
        <p>
            {{ _i('There are not categories.') }}
        </p>
        <p>
            {{ _i("Categories are collection of checklists, to assign them to referents in block. Click on 'Create New Category' to create a new one, and then create your first checklist!") }}
        </p>
        <p>
            {{ _i('It is required at least one category to create checklists.') }}
        </p>
    </div>
@else
    <div class="sortable" data-sorted-handler=".sorting-categories" data-sorted-callback="updateCategories" data-update-url="{{ route('category.update', 0) }}">
        @foreach($categories as $category)
            <div class="row category-row" data-category-id="{{ $category->id }}" data-update-url="{{ route('category.update', $category->id) }}">
                <div class="col-12 col-md-6">
                    <p class="display-5">{{ $category->name }}</p>
                    <span class="action-icons">
                        <span class="oi oi-pencil async-modal-edit" title="{{ _i('Edit Category') }}" data-edit-url="{{ route('category.edit', $category->id) }}"></span>
                        <span class="oi oi-trash async-modal-edit" title="{{ _i('Delete Category') }}" data-edit-url="{{ route('category.askdestroy', $category->id) }}"></span>
                        <span class="oi oi-resize-height sorting-categories" title="{{ _i('Drag to sort') }}"></span>
                    </span>
                </div>
                <div class="col-12 col-md-6">
                    <ul class="list-group sortable" data-sorted-callback="updateChecklists">
                        @foreach($category->checklists as $checklist)
                            <li class="list-group-item" data-checklist-id="{{ $checklist->id }}">
                                <span class="name">{{ $checklist->name }}</span>
                                <span class="action-icons float-end">
                                    <span class="oi oi-pencil async-modal-edit" title="{{ _i('Edit Checklist') }}" data-edit-url="{{ route('checklist.edit', $checklist->id) }}"></span>
                                    <span class="oi oi-trash async-modal-edit" title="{{ _i('Delete Checklist') }}" data-edit-url="{{ route('checklist.askdestroy', $checklist->id) }}"></span>
                                    <span class="oi oi-loop-square async-modal-edit" title="{{ _i('Duplicate Checklist') }}" data-edit-url="{{ route('checklist.create', ['original' => $checklist->id]) }}"></span>
                                    <span class="oi oi-resize-height" title="{{ _i('Drag to Sort') }}"></span>
                                </span>
                            </li>
                        @endforeach
                    </ul>

                    <button type="button" class="list-group-item list-group-item-action list-group-item-primary btn-sm" data-bs-toggle="modal" data-bs-target="#createChecklist-{{ $category->id }}">{{ _i('Add a Checklist in Category %s', [$category->name]) }}</button>
                    @include('checklist.edit', ['category' => $category, 'checklist' => null])
                </div>
            </div>
        @endforeach
    </div>
@endif

@include('category.edit', ['category' => null])
