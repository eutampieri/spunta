<div class="modal fade" id="destroyItem-{{ $item->id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ _i('Delete Item') }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{ route('item.destroy', $item->id) }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <div class="modal-body">
                    <p>
                        {{ _i("Are you really sure you want to delete item %s?", [$item->name]) }}
                    </p>
                    <p>
                        {{ _i('Will be removed from all checklists to which it is assigned.') }}
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ _i('Close') }}</button>
                    <button type="submit" class="btn btn-danger">{{ _i('Delete Item') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
