<div class="row">
    <div class="col">
        <button type="button" class="btn btn-primary float-end" data-bs-toggle="modal" data-bs-target="#createItem">{{ _i('Create New Item') }}</button>
        <button type="button" class="btn btn-primary float-end me-2" data-bs-toggle="modal" data-bs-target="#importCSV">{{ _i('Import CSV') }}</button>
        <div class="clearfix"></div>
        <hr>
    </div>
</div>

<div class="row">
    <div class="col">
        @if($items->isEmpty())
            <div class="alert alert-info">
                <p>
                    {{ _i('There are not items.') }}
                </p>
                <p>
                    {{ _i("Items can be assigned to checklists. Each is defined by a name and a type. Each can activate an email notification to referents when filled with configured values.") }}
                </p>
                <p>
                    {{ _i('The same item can be added to many different checklist.') }}
                </p>
                <p>
                    {{ _i('Available types are:') }}
                </p>
                <ul>
                    @foreach(App\Item::datatypes() as $datatype)
                        <li><strong>{{ $datatype->label }}</strong>: {{ $datatype->help }}</li>
                    @endforeach
                </ul>
            </div>
        @else
            <input type="text" class="form-control filter-table" data-target="#admin-items" placeholder="{{ _i('Filter') }}">
            <hr>

            <div class="table-responsive">
                <table class="table" id="admin-items">
                    <thead>
                        <tr>
                            <th width="25%">{{ _i('Name') }}</th>
                            <th width="35%">{{ _i('Help Text') }}</th>
                            <th width="20%">{{ _i('Type') }}</th>
                            <th width="5%">{{ _i('Notification') }}</th>
                            <th width="15%">{{ _i('Actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($items as $item)
                            <tr data-item-id="{{ $item->id }}">
                                <td class="filterable">
                                    {{ $item->name }}
                                </td>
                                <td>
                                    {{ $item->help }}
                                </td>
                                <td>
                                    {{ App\Item::datatype($item->type)->label }}
                                </td>
                                <td>
                                    @if($item->notify)
                                        <span class="oi oi-check" title="{{ _i('Notifications Enabled') }}"></span>
                                    @else
                                        <span class="oi oi-ban" title="{{ _i('Notifications Not Enabled') }}"></span>
                                    @endif
                                </td>
                                <td>
                                    <span class="action-icons">
                                        <span class="oi oi-pencil async-modal-edit" data-edit-url="{{ route('item.edit', $item->id) }}"></span>
                                        <span class="oi oi-trash async-modal-edit" data-edit-url="{{ route('item.askdestroy', $item->id) }}"></span>
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
</div>

@include('item.edit', ['item' => null])

<div class="modal fade" id="importCSV" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ _i('Import CSV') }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{ route('item.import') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="modal-body">
                    <p>
                        {{ _i('From here, it is possible to batch upload a list of items to be then assigned into individual checklists.') }}
                    </p>
                    <p>
                        {{ _i('The columns in the CSV document are defined as following:') }}
                    </p>
                    <ul>
                        @foreach(App\Item::importColumns() as $column)
                            <li>{{ $column->label }}: {{ $column->description }}</li>
                        @endforeach
                    </ul>
                    <p>
                        {{ _i('Already existing items whose name is found on the new list will be updated. Existing items not present in the list will be kept untouched.') }}
                    </p>
                    <p>
                        {!! _i('<a href="%s">Click here</a> to download a CSV template with a few sample lines.', [route('item.import.sample')]) !!}
                    </p>

                    <div class="form-group mt-4">
                        <input type="file" class="form-control" name="file" id="file" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ _i('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ _i('Save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
