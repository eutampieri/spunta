<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model implements MayHaveValue
{
    use SoftDeletes;

    public static function datatypes()
    {
        return [
            (object) [
                'value' => 'boolean',
                'label' => _i('True/False'),
                'notifiable' => true,
                'help' => _i("A simple selection to determine if the object is present or not (or if it has a specific attribute, defined in the name of the object)."),
            ],
            (object) [
                'value' => 'number',
                'label' => _i('Numeric'),
                'notifiable' => true,
                'help' => _i("A number to indicate a quantity, a level, a temperature or other attributes. If enabled, can generate a notification when the value if above or below a given threeshold."),
            ],
            (object) [
                'value' => 'text',
                'label' => _i('Short Text'),
                'notifiable' => true,
                'help' => _i("A short text field, to be used for a few words."),
            ],
            (object) [
                'value' => 'longtext',
                'label' => _i('Long Text'),
                'notifiable' => true,
                'help' => _i("A bigger text field, to be used for a note or a report."),
            ],
            (object) [
                'value' => 'datetime',
                'label' => _i('Date and Time'),
                'notifiable' => true,
                'help' => _i("Date and time, selectable by the operator."),
            ],
            (object) [
                'value' => 'select',
                'label' => _i('Choice'),
                'notifiable' => true,
                'help' => _i("To choose among a set of options, defined within the item. If enabled, can generate a notification when one or more specific options are selected."),
            ],
        ];
    }

    public static function importColumns()
    {
        $type_labels = [];
        foreach(Item::datatypes() as $datatype) {
            $type_labels[] = $datatype->label;
        }

        return [
            (object) [
                'label' => _i('Name'),
                'description' => _i('The name of the item'),
            ],
            (object) [
                'label' => _i('Help Text'),
                'description' => _i('An optional text to describe the item'),
            ],
            (object) [
                'label' => _i('Type'),
                'description' => _i('The type of the item. May be one of the following values: %s', [join(', ', $type_labels)]),
            ],
            (object) [
                'label' => _i('Options'),
                'description' => _i('Only if type is "Choice", define here a comma-separated list of selectable options'),
            ],
        ];
    }

    public static function datatype($type)
    {
        $types = self::datatypes();

        foreach($types as $t) {
            if ($t->value == $type) {
                return $t;
            }
        }

        return null;
    }

    public function evaluate($value)
    {
        $status = false;

        if ($this->notify == false) {
            return $status;
        }

        try {
            switch($this->type) {
                case 'boolean':
                    $status = ($value == $this->notificate->when);
                    break;

                case 'number':
                    if ($this->notificate->when == 'minor') {
                        $status = ($value <= $this->notificate->than);
                    }
                    else {
                        $status = ($value >= $this->notificate->than);
                    }
                    break;

                case 'select':
                    $status = in_array($value, $this->notificate->when);
                    break;

                case 'text':
                case 'longtext':
                    if ($this->notificate->when == 'empty') {
                        $status = empty($value);
                    }
                    else {
                        $status = !empty($value);
                    }

                    break;
            }
        }
        catch(\Exception $e) {
            \Log::error('Error in item evaluation: ' . $e->getMessage());
        }

        return $status;
    }

    public function getChoicesTextsAttribute()
    {
        $adjusted_contents = [];
        $ret = [];

        if ($this->type == 'select') {
            $data = json_decode($this->data);
            foreach($data as $d) {
                if (is_string($d)) {
                    $obj = (object)[
                        'text' => $d,
                        'notify' => false
                    ];

                    $adjusted_contents[] = $obj;
                    $d = $obj;
                }

                $ret[] = $d->text;
            }
        }

        if (!empty($adjusted_contents)) {
            $this->data = json_encode($adjusted_contents);
            $this->save();
        }

        return $ret;
    }

    public function getNotificateAttribute()
    {
        return json_decode($this->notification);
    }

    public function existingData($data)
    {
        if ($data) {
            foreach($data as $d) {
                if ($d->name == $this->name && isset($d->value) && filled($d->value)) {
                    switch($this->type) {
                        case 'boolean':
                            if ($d->value == 'false') {
                                return false;
                            }

                            break;

                        case 'select':
                            if ($d->value == _i('None Selected')) {
                                return false;
                            }

                            break;
                    }

                    return $d->value;
                }
            }
        }

        return false;
    }
}
