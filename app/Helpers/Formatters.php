<?php

function printableDateTime($date)
{
    return date('d/m/Y H:i', strtotime($date));
}

function printableDate($date)
{
    return date('d/m/Y', strtotime($date));
}
