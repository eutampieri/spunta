<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Log;
use PDF;
use Storage;

use ReportsDisk;

class History extends Model
{
    protected $dates = [
        'created_at',
        'updated_at',
        'date'
    ];

    public function user()
    {
        return $this->belongsTo('App\User')->withTrashed();
    }

    public function checklist()
    {
        return $this->belongsTo('App\Checklist')->withTrashed();
    }

    public function scopeCompleted($query)
    {
        $query->where('pending', false);
    }

    public function toPDF()
    {
        $filename = sprintf('report %s %s %s %s.pdf', $this->id, $this->date->toDateString(), $this->checklist->name, $this->user->name);

        if (global_multi_installation()) {
            $instance = instance_name();
            $path = $instance . '/' . $filename;
        }
        else {
            $path = $filename;
        }

        if (ReportsDisk::exists($path) == false) {
            $contents = nl2br($this->as_text);

            $logo_path = images_path() . 'logo.jpg';
            if (file_exists($logo_path)) {
                $contents = sprintf('<img src="%s"><br><br>', $logo_path) . $contents;
            }

            $pdf = PDF::loadHTML($contents)->output();
            ReportsDisk::put($path, $pdf);
        }

        return $path;
    }

    public function exportToJSON()
    {
        $filename = sprintf('report %s %s %s %s.json', $this->id, $this->date->toDateString(), $this->checklist->name, $this->user->name);

        if (global_multi_installation()) {
            $instance = instance_name();
            $path = $instance . '/' . $filename;
        }
        else {
            $path = $filename;
        }

        if (ReportsDisk::exists($path) == false) {
            $contents = json_encode([
                "id" => $this->id,
                "machine_readable" => json_decode($this->as_data, true),
                "signed" => $this->as_text,
            ]);
            ReportsDisk::put($path, $contents);
        }

        return $path;
    }
}
