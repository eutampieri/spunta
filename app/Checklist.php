<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Config;

class Checklist extends Model
{
    use SoftDeletes;

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function slots()
    {
        return $this->hasMany('App\Slot')->where('parent_id', 0)->orderBy('sorting');
    }

    public function histories()
    {
        return $this->hasMany('App\History')->orderBy('date', 'desc');
    }

    public function hasHour($day, $hour)
    {
        if (Config::getConfig('checklist_times') == false) {
            return false;
        }
        else {
            $info = json_decode($this->hours);
            if (isset($info->$day) == false) {
                return false;
			}
            else {
                return in_array($hour, $info->$day);
			}
        }
    }

    public function inTime($date = null)
    {
        if (Config::getConfig('checklist_times') == false) {
            return true;
		}

        if ($date == null) {
            $time = time();
        }
        else {
            $time = strtotime($date);
        }

		$today = mb_strtolower(date('D', $time));
		$now = date('H', $time);
        return $this->hasHour($today, sprintf('%02d:00', $now));
    }

    public static function availableDays()
    {
        return [
            'mon' => _i('Mon'),
            'tue' => _i('Tue'),
            'wed' => _i('Wed'),
            'thu' => _i('Thu'),
            'fri' => _i('Fri'),
            'sat' => _i('Sat'),
            'sun' => _i('Sun'),
        ];
    }

    public static function availableHours()
    {
        $ret = [];

        for($i = 0; $i < 24; $i++) {
            $ret[] = sprintf('%02d:00', $i);
		}

        return $ret;
    }
}
