<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Config;

class ConfigController extends Controller
{
    public function index()
    {
        return view('config.index');
    }

    public function update(Request $request, $id)
    {
        $this->permissionAccess('checklists');

        $conf = Config::where('name', 'checklist_times')->first();
        $checklist_times = $request->has('checklist_times');
        $conf->value = $checklist_times ? 'true' : 'false';
        $conf->save();

        $conf = Config::where('name', 'self_verification')->first();
        $self_verification = $request->has('self_verification');
        $conf->value = $self_verification ? 'true' : 'false';
        $conf->save();

        $conf = Config::where('name', 'notify_sync_reports')->first();
        $conf->value = trim($request->input('notify_sync_reports'));
        $conf->save();

        $conf = Config::where('name', 'language')->first();
        $conf->value = trim($request->input('language'));
        $conf->save();

        $conf = Config::where('name', 'intro_message')->first();
        $conf->value = trim($request->input('intro_message'));
        $conf->save();

        $ftp = (object) [
            'enable' => $request->has('enable_ftp_push'),
            'host' => trim($request->input('ftp_host')),
            'username' => trim($request->input('ftp_username')),
            'password' => trim($request->input('ftp_password')),
            'port' => trim($request->input('ftp_port')),
            'root' => trim($request->input('ftp_root')),
            'ssl' => $request->has('ftp_ssl'),
            'passive' => $request->has('ftp_passive'),
        ];

        $conf = Config::where('name', 'ftp_push')->first();
        $conf->value = json_encode($ftp);
        $conf->save();

        if ($request->hasFile('logo')) {
            $logo_path = images_path() . 'logo.jpg';
            file_put_contents($logo_path, file_get_contents($request->logo->path()));
        }

        /*

        $conf = Config::where('name', 'login')->first();
        $conf->value = trim($request->input('login'));
        $conf->save();

        $conf = Config::where('name', 'enforced_domain')->first();
        $conf->value = trim($request->input('enforced_domain'));
        $conf->save();

        */

        return redirect()->route('config.index');
    }

    public function getImage()
    {
        $logo_path = images_path() . 'logo.jpg';
        if (file_exists($logo_path)) {
            return response()->download($logo_path);
        }
        else {
            abort(404);
        }
    }
}
