<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Socialite;
use Session;
use Auth;

use App\Config;
use App\User;

class LoginController extends Controller
{
    public function index()
    {
        return view('login');
    }

    public function logout()
    {
        $user = Auth::user();
        if ($user == null) {
            return redirect()->route('login');
        }

        $mode = Config::getConfig('login');

        switch($mode) {
            case 'sso':
                if ($user->remember) {
                    return redirect()->route('login');
                }
                else {
                    Auth::logout(); // Logout of your app
    
                    return redirect(Socialite::driver('keycloak')->getLogoutUrl());
                }

                break;

            case 'manual':
                Auth::logout();
                return redirect()->route('login');
        }
    }

    public function manualLogin(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return redirect()->route('notice.index');
        }
        else {
            return redirect()->route('login');
        }
    }

    public function redirectToProvider(Request $request)
    {
        Session::put('remember_me', $request->has('remember'));
        return Socialite::driver('keycloak')->scopes(["openid"])->redirect();
    }

    private function ensureAccess($user)
    {
        $enforced_domain = Config::getConfig('enforced_domain');
        if (!empty($enforced_domain)) {
            $email = $user->getEmail();
            list($account, $domain) = explode('@', $email);
            if ($domain == $enforced_domain)
                return true;
            else
                return false;
        }

        return true;
    }

    public function handleProviderCallback()
    {
        $user = Socialite::driver('keycloak')->stateless()->user();

        $email = $user->getEmail();
        $u = User::where('email', $email)->first();
        if ($u == null) {
            if ($this->ensureAccess($user)) {
                $u = new User();
                $u->email = $user->getEmail();
                $u->name = $user->getName();
                $u->password = bin2hex(random_bytes(20));

                /*
                    First user is the admin
                */
                if (User::all()->count() == 0)
                    $u->permissions = json_encode(['user', 'checklists', 'users', 'notices', 'configs']);
                else
                    $u->permissions = json_encode(['user']);

                $u->save();
            }
            else {
                Session::flash('msg_type', 'error');
                Session::flash('msg', 'Not authorized');
                return redirect()->route('login');
            }
        }

        $u->remember = Session::get('remember_me');
        $u->save();

        Auth::loginUsingId($u->id, true);
        return redirect()->route('notice.index');
    }
}
