<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;
use App\Item;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->permissionAccess('checklists');
        $categories = Category::orderBy('sorting')->get();
        $items = Item::orderBy('name')->get();
        $active_tab = $request->input('tab', 'categories');
        $active_checklist = $request->input('checklist', '-1');
        return view('category.index', compact('active_tab', 'active_checklist', 'categories', 'items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->permissionAccess('checklists');

        $category = new Category();
        $category->name = $request->input('name');
        $category->save();

        return redirect()->route('category.index', ['tab' => 'categories']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->permissionAccess('checklists');
        $category = Category::findOrFail($id);
        return view('category.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->permissionAccess('checklists');
        $category = Category::findOrFail($id);
        return view('category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->permissionAccess('checklists');

        $saving_type = $request->input('saving_type', '');

        switch($saving_type) {
            case 'sort_categories':
                $index = 1;
                $ids = $request->input('ids');
                foreach ($ids as $id) {
                    $category = Category::findOrFail($id);
                    $category->sorting = $index;
                    $category->save();
                    $index++;
                }
                break;

            case 'sort_checklists':
                $category = Category::findOrFail($id);

                $index = 1;
                $ids = $request->input('ids');
                foreach ($ids as $id) {
                    $category->checklists()->where('id', $id)->update(['sorting' => $index]);
                    $index++;
                }
                break;

            default:
                $category = Category::findOrFail($id);
                $category->name = $request->input('name');
                $category->save();
                return redirect()->route('category.index', ['tab' => 'categories']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->permissionAccess('checklists');
        $category = Category::findOrFail($id);
        $category->checklists()->delete();
        $category->delete();
        return redirect()->route('category.index', ['tab' => 'categories']);
    }

    public function askdestroy($id)
    {
        $this->permissionAccess('checklists');
        $category = Category::findOrFail($id);
        return view('category.destroy', compact('category'));
    }
}
