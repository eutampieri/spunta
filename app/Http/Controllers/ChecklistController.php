<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Session;
use Auth;

use App\Category;
use App\History;
use App\Checklist;
use App\Slot;
use App\Item;
use App\Config;

class ChecklistController extends Controller
{
    public function index(Request $request)
    {
        $user_id = $request->user()->id;
        $categories = Category::orderBy('sorting', 'asc')->get();
        $pending = History::where('user_id', $user_id)->where('pending', true)->orderBy('created_at', 'desc')->get();
        return view('checklist.index', compact('categories', 'pending'));
    }

    public function create(Request $request)
    {
        $this->permissionAccess('checklists');
        $id = $request->input('original');
        $checklist = Checklist::findOrFail($id);
        return view('checklist.duplicate', compact('checklist'));
    }

    private function readHours($checklist, $request)
    {
        if (Config::getConfig('checklist_times')) {
            $hours = $request->input('hours');

            $to_save = (object)[];
            foreach(Checklist::availableDays() as $day => $label)
                $to_save->$day = [];

            foreach($hours as $h) {
                list($day, $hour) = explode('_', $h);
                $to_save->$day[] = $hour;
            }

            $checklist->hours = json_encode($to_save);
        }
        else {
            $checklist->hours = '[]';
        }
    }

    private function recursiveReplicate($oslot, $parent, $checklist)
    {
        $slot = $oslot->replicate();
        $slot->checklist_id = $checklist->id;
        $slot->parent_id = ($parent == null) ? 0 : $parent->id;
        $slot->save();

        foreach($oslot->items as $item) {
            $slot->items()->attach($item->id, ['sorting' => $item->pivot->sorting]);
        }

        foreach($oslot->slots as $subslot) {
            $this->recursiveReplicate($subslot, $slot, $checklist);
        }
    }

    public function store(Request $request)
    {
        $this->permissionAccess('checklists');

        $original_checklist = $request->input('original_checklist', null);

        if ($original_checklist == null) {
            $category_id = $request->input('category_id');
            $category = Category::findOrFail($category_id);

            $checklist = new Checklist();
            $checklist->name = $request->input('name');
            $checklist->category_id = $category_id;
            $checklist->notices_recipient = $request->input('notices_recipient');
            $checklist->sorting = $category->checklists()->count() + 1;
            $this->readHours($checklist, $request);
            $checklist->save();

            $slot = new Slot();
            $slot->name = 'Default';
            $slot->checklist_id = $checklist->id;
            $slot->save();
        }
        else {
            $original = Checklist::find($original_checklist);
            $checklist = $original->replicate();
            $checklist->name = $request->input('name');
            $checklist->save();

            foreach ($original->slots as $oslot) {
                $this->recursiveReplicate($oslot, null, $checklist);
            }
        }

        return redirect()->route('category.index', ['tab' => 'categories']);
    }

    public function show($id)
    {
        return redirect()->route('category.index', ['tab' => 'distribution', 'checklist' => $id]);
    }

    public function edit($id)
    {
        $this->permissionAccess('checklists');
        $checklist = Checklist::findOrFail($id);
        return view('checklist.edit', compact('checklist'));
    }

    public function update(Request $request, $id)
    {
        $this->permissionAccess('checklists');

        $checklist = Checklist::findOrFail($id);
        $saving_type = $request->input('saving_type', '');

        switch($saving_type) {
            case 'sort_slots':
                $index = 1;
                $ids = $request->input('ids');
                foreach ($ids as $id) {
                    $checklist->slots()->where('id', $id)->update(['sorting' => $index]);
                    $index++;
                }
                break;

            default:
                $checklist->name = $request->input('name');
                $checklist->notices_recipient = $request->input('notices_recipient');
                $this->readHours($checklist, $request);
                $checklist->save();
                return redirect()->route('category.index', ['tab' => 'categories']);
        }
    }

    public function destroy($id)
    {
        $this->permissionAccess('checklists');
        Checklist::findOrFail($id)->delete();
        return redirect()->route('category.index', ['tab' => 'categories']);
    }

    public function askdestroy($id)
    {
        $this->permissionAccess('checklists');
        $checklist = Checklist::findOrFail($id);
        return view('checklist.destroy', compact('checklist'));
    }
}
