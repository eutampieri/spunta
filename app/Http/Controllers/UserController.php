<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Hash;

use App\User;

class UserController extends Controller
{
    public function index()
    {
        $this->permissionAccess('users');
        $users = User::withTrashed()->orderBy('name')->get();
        $currentuser = Auth::user();
        return view('user.index', compact('users', 'currentuser'));
    }

    public function edit($id)
    {
        $this->permissionAccess('users');
        $user = User::findOrFail($id);
        return view('user.edit', compact('user'));
    }

    public function store(Request $request)
    {
        $this->permissionAccess('users');

        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->save();

        return redirect()->route('user.index');
    }

    public function update(Request $request, $id)
    {
        $this->permissionAccess('users');

        $user = User::findOrFail($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');

        $password = $request->input('password', '');
        if (!empty($password)) {
            $user->password = Hash::make($password);
        }

        $user->save();

        return redirect()->route('user.index');
    }

    public function destroy($id)
    {
        $this->permissionAccess('users');
        User::findOrFail($id)->delete();
        return redirect()->route('user.index');
    }

    public function askdestroy($id)
    {
        $this->permissionAccess('users');
        $user = User::findOrFail($id);
        return view('user.destroy', compact('user'));
    }

    public function setPermission(Request $request)
    {
        $this->permissionAccess('users');

        $user_id = $request->input('target_id');
        $user = User::findOrFail($user_id);

        $flag = $request->input('flag');
        $permission = $request->input('permission');

        $current = json_decode($user->permissions);

        if($flag == 'true') {
            if (!in_array($permission, $current))
                $current[] = $permission;
        }
        else {
            $new_current = [];
            foreach($current as $c)
                if ($c != $permission)
                    $new_current[] = $c;

            $current = $new_current;
        }

        $user->permissions = json_encode($current);

        $user->save();
    }

    public function generatekeys()
    {
        $user = Auth::user();
        $no_menu = true;
        return view('user.keys', compact('user', 'no_menu'));
    }

    public function savekeys(Request $request)
    {
        $user = Auth::user();
        $user->saveKeys($request->input('public'), $request->input('private'));
        return redirect()->route('checklist.index');
    }

    public function getkey(Request $request, $id)
    {
        $myself = Auth::user();

        if ($myself->id != $id && $myself->hasPermission('users') == false)
            abort(403);

        $user = User::findOrFail($id);
        $key = $user->getPrivkeyAttribute();

        $filename = preg_replace('/[^a-zA-Z0-9]/', '', $user->name) . '.key';

        header('Content-Type: text/plain');
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        header('Cache-Control: no-cache, no-store, must-revalidate');
        header('Pragma: no-cache');
        header('Expires: 0');
        return $key;
    }

    public function askkeydestroy($id)
    {
        $myself = Auth::user();

        if ($myself->id != $id && $myself->hasPermission('users') == false)
            abort(403);

        $user = User::findOrFail($id);
        return view('user.keydestroy', compact('user'));
    }

    public function deletekeys($id)
    {
        $myself = Auth::user();

        if ($myself->id != $id && $myself->hasPermission('users') == false)
            abort(403);

        $user = User::findOrFail($id);
        $user->deleteKeys();
        return redirect()->route('user.index');
    }

    public function restore($id)
    {
        $this->permissionAccess('users');
        User::withTrashed()->findOrFail($id)->restore();
        return redirect()->route('user.index');
    }

    public function import(Request $request)
    {
        $this->permissionAccess('users');

        if ($request->hasFile('file') && $request->file('file')->isValid()) {
            $tmp = sys_get_temp_dir();
            $path = sprintf('%s/import.csv', $tmp);
            $request->file('file')->move($tmp, 'import.csv');

            $f = fopen($path, 'r+');

            /*
                Skip first row
            */
            $row = fgetcsv($f);

            while($row = fgetcsv($f)) {
                $user = new User();
                $user->name = $row[0];
                $user->email = $row[1];
                $user->password = $row[2];
                $user->save();
            }

            unlink($path);
        }

        return redirect()->route('user.index');
    }
}
