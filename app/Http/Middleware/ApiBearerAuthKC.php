<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Socialite;
use App\User;
use Illuminate\Http\Request;

class ApiBearerAuthKC
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->bearerToken();
        if($token === null) {
            return response('Missing Bearer token', 401);
        }
        $user = Socialite::driver('keycloak')->userFromToken($token);
        $email = $user->getEmail();
        $u = User::where('email', $email)->first();
        if ($u == null) {
            return response('Sign up first', 401);
        }

        Auth::loginUsingId($u->id, true);

	return $next($request);
    }
}
