<?php

namespace App;

interface MayHaveValue
{
	public function existingData($data);
}
