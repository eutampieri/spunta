<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class KeysReset extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $key_id;

    public function __construct($user, $key_id)
    {
        $this->user = $user;
        $this->key_id = $key_id;
    }

    public function build()
    {
        return $this->subject(_i('keys reset for %s', $this->user->name))->replyTo($this->user->email)->text('email.keysreset');
    }
}
