<?php

use App\Category;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth.bearer:api')->group(function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::get('/checklists', function (Request $request) {
        $res = [];
        $walk = function($slot, $current_slot_stack) use ( &$walk ) {
            $r = [];
            foreach($slot->getContentsAttribute() as $i) {
                if(is_a($i, 'App\Slot')) {
                    $r = array_merge($r, $walk($i, array_merge($current_slot_stack, [$i->name])));
                } else {
                    $field = [
                        "id" => $i->id,
                        "name" => $i->name,
                        "type" => $i->type,
                        "help" => $i->help,
                        "slot" => ["name" => $current_slot_stack, "id" => $slot->id],
                    ];
                    if($field["type"] == "select") {
                        $field["options"] = $i->choices_texts;
                    }
                    array_push($r, $field);
                }
            }
            return $r;
        };

        foreach(Category::orderBy('sorting', 'asc')->get() as $category) {
            foreach($category->checklists as $checklist) {
                $c = [
                    "category" => $category->name,
                    "title" => $checklist->name,
                    "id" => $checklist->id,
                    "items" => [],
                ];
                foreach($checklist->slots as $slot) {
                    $c["items"] = array_merge($c["items"], $walk($slot, [$slot->name]));
                }
                array_push($res, $c);
            }
        }
        return $res;
    });
    Route::resource('history', 'HistoryController');
});
