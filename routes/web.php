<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'LoginController@index')->name('login');
Route::get('logout', 'LoginController@logout')->name('logout');
Route::post('login/manual', 'LoginController@manualLogin')->name('login.manual');
Route::post('login/sso', 'LoginController@redirectToProvider')->name('login.sso');
Route::get('login/sso/callback', 'LoginController@handleProviderCallback');

/*
    Those are routes used to enforce consistency on the flow: all users must
    have valid keys, all reports must be signed
*/
Route::middleware('auth')->group(function () {
    Route::get('user/keys', 'UserController@generatekeys')->name('user.keys');
    Route::post('user/keys', 'UserController@savekeys')->name('user.savekeys');
    Route::get('user/getkey/{id}', 'UserController@getkey')->name('user.getkey');
    Route::get('user/{id}/keys/delete', 'UserController@askkeydestroy')->name('user.askkeydestroy');
    Route::delete('user/{id}/keys', 'UserController@deletekeys')->name('user.deletekeys');

    Route::get('history/{id}/sign', 'HistoryController@sign')->name('history.sign');
    Route::get('history/export', 'HistoryController@export')->name('history.export');

    Route::resource('history', 'HistoryController');
});

Route::middleware(['auth', 'enforce_flow'])->group(function () {
    Route::get('/', function() {
        return redirect()->route('notice.index');
    });

    Route::get('config/image', 'ConfigController@getImage')->name('config.image');

    Route::post('user/setpermission', 'UserController@setPermission')->name('user.setpermission');
    Route::get('user/{id}/delete', 'UserController@askdestroy')->name('user.askdestroy');
    Route::get('user/{id}/restore', 'UserController@restore')->name('user.restore');
    Route::post('user/import', 'UserController@import')->name('user.import');

    Route::get('category/{id}/delete', 'CategoryController@askdestroy')->name('category.askdestroy');

    Route::get('slot/{id}/delete', 'SlotController@askdestroy')->name('slot.askdestroy');
    Route::post('slot/{id}/attach', 'SlotController@attach')->name('slot.attach');

    Route::get('checklist/{id}/delete', 'ChecklistController@askdestroy')->name('checklist.askdestroy');

    Route::get('item/{id}/delete', 'ItemController@askdestroy')->name('item.askdestroy');
    Route::post('item/import', 'ItemController@import')->name('item.import');
    Route::get('item/import/sample', 'ItemController@importSample')->name('item.import.sample');

    Route::get('history/{id}/download', 'HistoryController@download')->name('history.download');

    Route::get('notice/admin', 'NoticeController@admin')->name('notice.admin');
    Route::get('notice/{id}/readcount', 'NoticeController@readcount')->name('notice.readcount');
    Route::get('notice/archive', 'NoticeController@archive')->name('notice.archive');
    Route::post('notice/{id}/read', 'NoticeController@read')->name('notice.read');
    Route::get('notice/{id}/delete', 'NoticeController@askdestroy')->name('notice.askdestroy');

    Route::resource('config', 'ConfigController');
    Route::resource('notice', 'NoticeController');
    Route::resource('user', 'UserController');
    Route::resource('category', 'CategoryController');
    Route::resource('slot', 'SlotController');
    Route::resource('checklist', 'ChecklistController');
    Route::resource('item', 'ItemController');
});
