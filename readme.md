# Spunta

Spunta is a checklists management tool.

Originally developed for a local Red Cross operative group, to review sanitary equipment at the beginning of each working turn.

For hosting service, special requests or for a demo, visit the website https://spunta.app/

## Installation

```
git clone https://gitlab.com/madbob/spunta.git
cd spunta
composer install
cp .env.example .env
# Edit .env with your configurations. Please read before the Advanced / Multi Tenancy paragraph below
php artisan key:generate
npm install
npm run prod
```

## Advanced

Many behaviors can be configured directly from the "Configurations" panel, but other are not explicitely exposed and require a bit more tuning.

### Reports Storage

It is possible to store the generated PDFs of reports both on local disk (default) or on a S3-compatible object storage (Scaleway is recommended).

Look at the `REPORTS_STORAGE` and following `STORAGE_*` options in the `.env.example` file, and in `config/filesystems.php` for even further configurations.

### Multi Tenancy

By default here is enabled a multi-tenancy configuration, where each instance has his own domain (e.g. first.spunta.app and second.spunta.app) and his own .env file (e.g. .env.first and .env.second). Different instances are routed using the third level domain of the requests and files are stored on different folders (each implicitely named with the same instance name).

To adopt a more classic setup (a single instance for a single deployment) access the file in `app/Helpers/Paths.php` and change the return value of `global_multi_installation()` from `true` to `false`. This will fallback to classic .env file evaluation.

### Google Login

It is possible to enable Google sign-in instead of locally generated accounts. This may be useful for organizations already using Google Workspace.

To enable it:

 * in the `configs` table of the database, alter the value of the row with `name = 'login'` and set it to `google`. It is also suggested to alter the value of the row with `name = 'enforced_domain'` to the own Google Workspace domain, to avoid granting access to everyone using a generic Google account
 * on your Google Developers Console, generate a OAuth 2.0 ClientID and Secret with proper configurations and save them in the .env file (see `GOOGLE_CLIENT_ID` and `GOOGLE_CLIENT_SECRET`)
