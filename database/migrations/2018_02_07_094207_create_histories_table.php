<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoriesTable extends Migration
{
    public function up()
    {
        Schema::create('histories', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->datetime('date');
            $table->integer('user_id')->unsigned();
            $table->integer('checklist_id')->unsigned();
            $table->boolean('status')->default(true);
            $table->boolean('signed')->default(false);
            $table->boolean('pending')->default(false);
            $table->text('as_data');
            $table->text('as_text');
        });
    }

    public function down()
    {
        Schema::dropIfExists('histories');
    }
}
