<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('name');
            $table->string('help')->default('');
            $table->enum('type', ['boolean', 'number', 'text', 'longtext', 'datetime', 'select']);
            $table->text('data')->nullable();
            $table->boolean('notify');
            $table->text('notification')->nullable();
        });

        Schema::create('item_slot', function (Blueprint $table) {
            $table->integer('slot_id')->unsigned();
            $table->integer('item_id')->unsigned();
            $table->integer('sorting')->unsigned()->default(0);

            $table->primary(['slot_id', 'item_id']);

            $table->foreign('slot_id')->references('id')->on('slots')->onDelete('cascade');
            $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('item_slot');
        Schema::dropIfExists('items');
    }
}
